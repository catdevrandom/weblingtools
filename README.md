# LingTools Web App - A front-end for the module "Lingtools - Linguistic Tools for Cognitive Science"

This is a Flask web app that interfaces the Lingtools module (https://bitbucket.org/catdevrandom/lingtools). The Lingtools module needs to be installed in the PYTHONPATH.

## Getting Started

To use this module, download the code using `git clone` and place it in your project.

Make sure you have the requisites installed. You can use the file `requirements.txt` to install the dependences using pip.

```
pip install -r requirements.txt
```

### Prerequisites

Software:

- Python 2.7 (not tested with Python 3).

Python packages (and their dependencies):

- lingtools (https://bitbucket.org/catdevrandom/lingtools)
- Flask
- Flask-Bootstrap
- flask-nav
- pytest
- requests
- simplejson
- six
- regex
- unicodecsv
- unidecode

### Configuration

Edit the file `weblingtools/config.py` to reflect the paths in your server.

Make sure the `UPLOAD_FOLDER` does not allow for executing scripts, as an extra caution against attacks.

Check `MEMORY_LIMIT` and set an appropriate value to prevent server crashes when dealing with too large files.


### Using Vagrant

This repository comes with a Vagrant file that can be used to easily set up a virtual server already configured to run Weblingtools. 

You will need to install Vagrant.

In Windows:

Open terminal (CMD), and type:

```
> d:
> cd Dev\weblingtools
> vagrant up
```

Log in to the server box via `vagrant ssh` or via GitBash:

```
> ssh 127.0.0.1 -l vagrant -p 2222
PASSWORD: vagrant
```

Run the test server:

```
$ source activate server-env
$ cd /var/www/vagrant_data/weblingtools
$ python run.py
```

The server is now running and the app can be accessed at: http://localhost:5000.



## Author

* **Maira B. Carvalho** (m.brandaocarvalho@uvt.nl), Tilburg University


## License

This project is licensed under the MIT License.


The MIT License

Copyright (c) 2017 Maira Brandao Carvalho, Tilburg University

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
