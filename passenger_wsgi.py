import sys, os
INTERP = os.path.join(os.environ['HOME'], '.pyenv', 'versions', 'myenv', 'bin', 'python')
print(INTERP)
if sys.executable != INTERP:
        os.execl(INTERP, INTERP, *sys.argv)


# important locations to add to path
BASEDIR = os.path.abspath(os.path.dirname(__name__))
APP_DIR = os.path.join(BASEDIR, 'weblingtools')
#LINGTOOLS_LIB = os.path.join(APP_DIR, 'lib', 'lingtools')

sys.path.append(APP_DIR)
#sys.path.append(LINGTOOLS_LIB)

from weblingtools_app import create_app

application = create_app()



# Uncomment next two lines to enable debugging
#from werkzeug.debug import DebuggedApplication
#application = DebuggedApplication(application, evalex=True)

#from flask import Flask
#application = Flask(__name__)

#@application.route('/')
#def index():
#       return 'Hello passenger'

