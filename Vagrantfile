# -*- mode: ruby -*-
# vi: set ft=ruby :

$script_1 = <<SCRIPT
#!/bin/bash

# Variables

DBHOST=localhost
DBNAME=dbname
DBUSER=dbuser
DBPASSWD=rootpass
DBPORT=3306

cd /home/vagrant
sudo chown -R vagrant:vagrant /home/vagrant

# Updating repository
sudo apt-get -y update

# Installing git
sudo apt-get install git

# Install pip
sudo apt-get -y install python-pip 

# Install unzip
sudo apt-get -y install unzip

SCRIPT

$script_2 = <<SCRIPT

# Install conda
cd /home/vagrant
wget -q https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O /home/vagrant/miniconda.sh
bash /home/vagrant/miniconda.sh -b -p /home/vagrant/miniconda
export PATH="/home/vagrant/miniconda/bin:$PATH"
echo "export PATH=/home/vagrant/miniconda/bin:$PATH" >> /home/vagrant/.bashrc

# Create a virtual environment
conda create --name server-env -y python=2.7 pip cython numpy scipy scikit-learn nltk gensim 
source activate server-env
pip install --upgrade pip

# Install lingtools
cd /home/vagrant
mkdir src
cd src
rm -Rf lingtools
git clone https://catdevrandom@bitbucket.org/catdevrandom/lingtools.git
cd lingtools
python setup.py install

# Install all dependencies for the Flask app
cd /var/www/vagrant_data/weblingtools
pip install -r requirements.txt

# Download lingtools assets
cd /home/vagrant/
mkdir .lingtools
cd .lingtools
rm -Rf assets
wget -q https://surfdrive.surf.nl/files/index.php/s/2FVDToX8NwmhHpr/download -O assets.zip
unzip assets.zip
rm assets.zip

# Download NLTK assets
mkdir /home/vagrant/nltk_data
cd /home/vagrant/nltk_data
python -m nltk.downloader maxent_ne_chunker brown sentiwordnet stopwords wordnet words vader_lexicon averaged_perceptron_tagger maxent_treebank_pos_tagger punkt -d /home/vagrant/nltk_data


SCRIPT



Vagrant.configure("2") do |config|

  config.vm.box = "bento/ubuntu-16.04"

  config.vm.network "forwarded_port", guest: 5000, host: 5000

  config.vm.synced_folder "./", "/var/www/vagrant_data", create: false


  config.vm.provider "virtualbox" do |vb|
     vb.memory = "5400"
  end


  config.vm.provision "shell", inline: $script_1

  config.vm.provision "shell", inline: $script_2, privileged: false
end
