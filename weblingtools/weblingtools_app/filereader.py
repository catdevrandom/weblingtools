'''
Created on May 30, 2017

@author: maira
'''

from __future__ import unicode_literals
from __future__ import absolute_import

import io 

import collections

#Logging
import logging
LOG = logging.getLogger(__name__)


class FileReader(object):
    '''
    Helper to read documents from a simple text file, allowing us to iterate 
    through the contents instead of loading everything to memory at once.
    '''
        
    def __init__(self, filename, *args, **kwargs):
        '''
        Initialize object that will help process a file. Requires a valid filename.
        :param filename: The file to read
        :param logger_interval: The interval in which logging messages will be output. Default: 1000
        :param document_boundary: The regex pattern used to split the file. Default: \n (new line)
        :param chunk_size: The file chunk size to read at each iteration
        '''
        
        self.filename = filename
        
        defaults = {
            'language': 'en',
            'document_boundary': "\n",
            'encoding': 'utf-8',
            'logger_interval': 1,
            'chunk_size': 1048576, # 1024*1024 = 1Mb
            }
        defaults.update(kwargs)
        
        # language codes dict
        lang_names = {
            'en': ('eng', 'english'),
            'nl': ('nld', 'dutch'),
            'ru': ('rus', 'russian'),
            }
        
        # Set language codes and name
        try:
            self.lang_2letter_code = defaults['language']
            self.lang_3letter_code = lang_names[defaults['language']][0]
            self.lang_name = lang_names[defaults['language']][1]
            
        except KeyError:
            LOG.warning("Unrecognized language %s. Using English as default.", self.lang_code)
            self.lang_name = 'english'
            self.lang_3letter_code = 'eng' 
        
        # Iterate through the updated dictionary setting the values in self
        for attr, value in defaults.items():
            setattr(self,attr,value)
        

        LOG.debug("Class attributes: %s", vars(self))
        
        
            
        
    def raw_documents(self):
        '''
        Iterate through the documents in the file without processing them.
        '''
        i = 0
        
        
        doc_queue = collections.deque([''])
        # Add an empty doc to queue
        
        with io.open(self.filename, mode='rt', encoding=self.encoding, errors = 'strict') as f:
            
            for filechunk in iter(lambda: f.read(self.chunk_size), ""):
                i += 1
                # Logging info
                if i % self.logger_interval == 0:
                        LOG.debug("Reading chunk #{}".format(i))

                # Split the chunk using the document boundary
                docs = filechunk.split(self.document_boundary)
                LOG.debug(docs)
                
                # Append the first document in the chunk to the last doc 
                # in the queue, popping it from the doc list
                doc_queue[-1] = doc_queue[-1] + docs.pop(0)
                
                # Cycle over remaining docs in the chunk and append to the queue. 
                for d in docs:
                    #Check if doc is not empty
                    if len(d)>0:            
                        doc_queue.append(d)
                    
                # Yield all but the last doc in the queue (the last one will
                # probably be incomplete
                while len(doc_queue)>=2:
                    yield doc_queue.popleft()

            # Now we cycled over all the chunks, so we yield the last doc in the queue
            else:
                yield doc_queue.popleft()
