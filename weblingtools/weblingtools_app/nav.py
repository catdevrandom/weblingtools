'''
Sets up navigation in the dashboard. Created on Apr 21, 2017

@author: maira 
'''
from __future__ import unicode_literals
from __future__ import absolute_import

from flask_nav import Nav
from flask_nav.elements import *

# To keep things clean, we keep our Flask-Nav instance in here. We will define
# frontend-specific navbars in the respective frontend, but it is also possible
# to put share navigational items in here.


nav = Nav()

nav.register_element('top', Navbar(
         Link('Home', dest='/'),
         Link('Simple analysis', dest='/simple'),
         Link('File analysis', dest='/file'),
         Link('Dynamic themes', dest='/dynamicthemes'),
         Link('Distinctive words', dest='/distinctivewords'),
         Link('Cosine distances', dest='/cosinedist'),
         Link('Nearest neighbors', dest='/neighbors'),
         Link('Help', dest='/help'),
         )
)