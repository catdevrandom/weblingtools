# encoding: utf-8
'''
Created on 12 apr. 2017

@author: brandaoc
'''
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import io
import uuid

import config

from lingtools.featureextractor import FeatureExtractor
from lingtools.dynamicthemes import DynamicThemes
from lingtools.cosinedists import CosineDists
from lingtools.freqanalyzer import FreqAnalyzer
from lingtools.tokenizer import LocalTokenizer
from lingtools.util import get_vectorspaces

import pandas as pd

#Logging
import logging
LOG = logging.getLogger(__name__)

# Use all available modules
#MODULES = None
MODULES = [
           "anew", 
           "basic", 
           "biber", 
           "extra", 
           #"lcm", 
           "mrc", 
           "ner", 
           "semanticvectors", 
           "sentimentanalysis"
           ]

###############################################
# Public functions

TOKENIZER_SETTINGS = {"language":"english", 
                      "encoding": "utf-8", 
                      "tagger": "nltk", 
                      "stanford_url": None, 
                      "simple_tokenizer": False, 
                      "remove_stopwords": True, 
                      "stopwords": None, # Use NLTK default stopwords 
                      "replace_digits": True, 
                      "remove_punctuation": True, 
                      "replace_not": True, 
                      "lowercase": False, 
                      "use_lemmas": True}

TOKENIZER_SETTINGS_DISTINCTIVEWORDS = {"language":"english", 
                      "encoding": "utf-8", 
                      "tagger": "nltk", 
                      "stanford_url": None, 
                      "simple_tokenizer": False, 
                      "remove_stopwords": False, 
                      "stopwords": None, # Use NLTK default stopwords 
                      "replace_digits": False, 
                      "remove_punctuation": True, 
                      "replace_not": False, 
                      "lowercase": False, 
                      "use_lemmas": True}

FEATURE_EXTRACTOR_SETTINGS = {"features": None, # deprecated 
                              "feature_sets": MODULES, 
                              "addons": None, 
                              "logging_interval": config.LOG_INTERVAL
                              }

DYNTHEMES_SETTINGS = {"logging_interval": config.LOG_INTERVAL
                      }

FREQANALYZER_SETTINGS = {"logging_interval": config.LOG_INTERVAL}

COSDIST_SETTINGS = {"logging_interval": config.LOG_INTERVAL}


def process_text(input_string, vectorspace_name=None):
    '''
    Processes a single text informed by the user.
    
    :param input_string:
    :param vectorspace_name:
    '''    
    
    # Create a Tokenizer object
    my_tokenizer_settings = TOKENIZER_SETTINGS
    tokenizer_obj = LocalTokenizer(**my_tokenizer_settings)
    
    # Load the FeatureExtractor object with the module for the desired features
    my_feature_extractor_settings = FEATURE_EXTRACTOR_SETTINGS
    my_feature_extractor_settings.update({"tokenizer_obj": tokenizer_obj,
                                          "vectorspace_name": vectorspace_name})
    ftextr_obj = FeatureExtractor(**my_feature_extractor_settings)     
    
    
    #LOG.info("Processing one doc...")    
    if len(input_string)>0:
        
        string_length = len(input_string)
        
        LOG.info("Simple text analysis request: length %s", string_length)
        
        if string_length <= config.MAX_TEXT_SIZE:
        
            # Feed the document to the object as a list of one document
            ftextr_obj.load_list([input_string])
            
            results = []
            # Get the resulting FeaturesContainer object
            for _, r in ftextr_obj.process():
                results.append(r)
                
            # This was a single doc, we only need the first item in the list
            result_obj = results[0]
        
            # Return the container structure as a dictionary    
            result_dict = result_obj.get_results()     
            
            return result_dict
        else:
            raise ValueError("Text too long! Maximum {} characteres.".format(config.MAX_TEXT_SIZE))
    
    else:
        raise ValueError("Empty string")  
    

def process_file(input_filename, filetype, encoding=None, vectorspace_name=None): 
    '''
    Processes a file, considering the filetype
    :param input_filename:
    :param filetype (int):  1: one document per line, no paragraphs; 
                            2: Single document, new line is a new paragraph
                            3: Single document, empty line is a new paragraph 
    '''
    # First, check if file is not empty
    if os.stat(input_filename).st_size == 0:
        raise ValueError('Empty file')
    else:
        
        LOG.info("File analysis request: filename %s; filetype %s, encoding %s", 
                 input_filename,  filetype, encoding)
        
        

        # Create a Tokenizer object
        my_tokenizer_settings = TOKENIZER_SETTINGS
        my_tokenizer_settings.update({"encoding": encoding})
        tokenizer_obj = LocalTokenizer(**my_tokenizer_settings)
        
        # Load the FeatureExtractor object with the module for the desired features
        my_feature_extractor_settings = FEATURE_EXTRACTOR_SETTINGS
        my_feature_extractor_settings.update({"tokenizer_obj": tokenizer_obj,
                                          "vectorspace_name": vectorspace_name})
        ftextr_obj = FeatureExtractor(**my_feature_extractor_settings)        
    
                
        # Load feature extractor according to file type
        if filetype==1: # One document per line, no paragraphs             
            ftextr_obj.load_file(input_filename, file_type='txt_multiple', encoding=encoding)
            
        elif filetype==2: # Whole file is one single document, new line is a new paragraph            
            ftextr_obj.load_file(input_filename, file_type='txt_single', paragraph_sep="\n", encoding=encoding)
        
        elif filetype==3: # Whole file is one single document, empty line is a new paragraph            
            ftextr_obj.load_file(input_filename, file_type='txt_single', paragraph_sep="\n\n", encoding=encoding)
            
        elif filetype==4: # Zipped file with multiple txt files, each is one single document, empty line is a new paragraph 
            raise NotImplementedError("Sorry, this function has not been implemented yet.")
        
        else:
            raise ValueError("Unknown file type!")
            
            
        # Generate a unique filename
        unique_filename = str(uuid.uuid4())    
        out_filename = os.path.join(config.RESULTS_FOLDER, unique_filename)
        
        
        # Create result file with header
        with io.open(out_filename, 'wt') as f_out: 
            feature_list = ftextr_obj.get_feature_names()
            LOG.debug("Formatted feature list to go to csv: %s", feature_list)
            header_line = ";".join(feature_list)
            f_out.write(header_line+"\n")  
        
        # Process!        
        for _, r in ftextr_obj.process():
            # Get a result tuple from the object: (group_id, group_code, feature_id, feature_code, feature_value)
            result_tuples = r.get_results_as_tuples()
            
            # Format the line
            result_line = ';'.join([str(x[4]) for x in result_tuples])
            
            # Write the result
            with io.open(out_filename, 'at') as f_out:        
                f_out.write(result_line + "\n")
            
        
        return unique_filename 

    

def process_dynamic_themes(theme_name, theme_words, input_filename, filetype, encoding=None, vectorspace_name=None): 
    '''
    Creates a dynamic theme using user-given theme words and
    returns a cosine distance value for the given text.
    
    :param theme_name:
    :param theme_words:
    :param input_filename:
    :param filetype:
    :param encoding:
    '''
    # First, check if file is not empty
    if os.stat(input_filename).st_size == 0:
        raise ValueError('Empty file')
    else:
        try:
            len_themewords = len(theme_words)
        except Exception:
            len_themewords = None
            
        LOG.info("Dynamic theme analysis request: filename %s; %s theme words, filetype %s, encoding %s", 
                 input_filename, len_themewords, filetype, encoding)
        
        if len_themewords <= config.MAX_NUMBER_THEME_WORDS:
            
            # Create a Tokenizer object
            my_tokenizer_settings = TOKENIZER_SETTINGS
            my_tokenizer_settings.update({"encoding": encoding})
            tokenizer_obj = LocalTokenizer(**my_tokenizer_settings)
            
            # Load the object with the module for processing the themes
            my_dynthemes_settings = DYNTHEMES_SETTINGS
            my_dynthemes_settings.update({"tokenizer_obj": tokenizer_obj,
                                          "vectorspace_name": vectorspace_name})
            dyntheme_obj = DynamicThemes(**my_dynthemes_settings)
            
            # Load the theme
            dyntheme_obj.load_theme(theme_name, theme_words, add_theme_name_to_list = False, remove_duplicates = True)
    
            # Load object according to file type
            if filetype==1: # One document per line, no paragraphs             
                dyntheme_obj.load_file(input_filename, file_type='txt_multiple', encoding=encoding)
                
            elif filetype==2: # Whole file is one single document, new line is a new paragraph            
                dyntheme_obj.load_file(input_filename, file_type='txt_single', paragraph_sep="\n", encoding=encoding)
            
            elif filetype==3: # Whole file is one single document, empty line is a new paragraph            
                dyntheme_obj.load_file(input_filename, file_type='txt_single', paragraph_sep="\n\n", encoding=encoding)
                
            elif filetype==4: # Zipped file with multiple txt files, each is one single document, empty line is a new paragraph 
                raise NotImplementedError("Sorry, this function has not been implemented yet.")
            
            else:
                raise ValueError("Unknown file type!")
                
                
            # Generate a unique filename
            unique_filename = str(uuid.uuid4())    
            out_filename = os.path.join(config.RESULTS_FOLDER, unique_filename)
            
            
            # Create result file with header
            with io.open(out_filename, 'wt') as f_out: 
                allwords = ";".join(["{}_cos;{}_count".format(x,x) for x in dyntheme_obj.get_theme_words()])
                header_line = "ID;theme_cos;theme_count;"+allwords
                LOG.debug("Header line: %s", header_line)
                f_out.write(header_line+"\n")  
            
            # Process!
            sep = ";"
            for doc_id, r in dyntheme_obj.process():
                result_line = [doc_id]
                for _,v,c in r:
                    result_line.append(v)
                    result_line.append(c)
                    
                result_line = sep.join([str(x) for x in result_line])
                LOG.debug("Result line: %s", result_line)
                # Write the result
                with io.open(out_filename, 'at') as f_out:        
                    f_out.write(result_line + "\n")
                
            
            return unique_filename
        
        else:
            raise ValueError("Too many theme words! Inform at most {} theme words.".format(config.MAX_NUMBER_THEME_WORDS)) 
        
    
def get_distinctive_words(file1_path, file2_path, file1_type=None, file2_type=None, file1_encoding=None, file2_encoding=None, vectorspace_name=None):
    this_file1_encoding = file1_encoding or 'auto'
    this_file2_encoding = file2_encoding or 'auto'
    this_file1_type = file1_type or 1
    this_file2_type = file2_type or 1
    
    LOG.info("Distinctive word analysis request. File1 %s; file1 type %s, file1 encoding %s. File2 %s; file2 type %s, file2 encoding %s. ", 
                 file1_path, file1_type, file1_encoding, file2_path, file2_type, file2_encoding)
    
    if file1_encoding != file2_encoding:
        raise ValueError("Please use the same encoding for both files")
    
    # First, check if file is not empty
    if os.stat(file1_path).st_size == 0:
        raise ValueError('Empty file: {}'.format(file1_path))
    elif os.stat(file2_path).st_size == 0:
        raise ValueError('Empty file: {}'.format(file2_path))
    else:
        
        
        # Create a Tokenizer object
        my_tokenizer_settings = TOKENIZER_SETTINGS_DISTINCTIVEWORDS
        my_tokenizer_settings.update({"encoding":  file1_encoding})
        tokenizer_obj = LocalTokenizer(**my_tokenizer_settings)
        
        # Load the object with the module for comparing documents
        my_freqanalyzer_settings = FREQANALYZER_SETTINGS
        my_freqanalyzer_settings.update({"tokenizer_obj": tokenizer_obj})
        LOG.info("Init parameters: %s", my_freqanalyzer_settings)
        freqanalyzer_obj = FreqAnalyzer(**my_freqanalyzer_settings)
        
        processed_file1_type = 'txt_multiple'
        processed_file1_paragsep = None
        processed_file2_type = 'txt_multiple'
        processed_file2_paragsep = None
        
        # Prepare file 1 type
        if this_file1_type==1: # One document per line, no paragraphs             
            processed_file1_type = 'txt_multiple'
            processed_file1_paragsep = None
        elif this_file1_type==2: # Whole file is one single document, new line is a new paragraph
            processed_file1_type = 'txt_single'
            processed_file1_paragsep = "\n"     
        elif this_file1_type==3: # Whole file is one single document, empty line is a new paragraph            
            processed_file1_type = 'txt_single'
            processed_file1_paragsep = "\n\n"            
        elif this_file1_type==4: # Zipped file with multiple txt files, each is one single document, empty line is a new paragraph 
            raise NotImplementedError("Sorry, this function has not been implemented yet.")        
        else:
            raise ValueError("Unknown file type!")
        
        # Prepare file 2 type
        if this_file2_type==1: # One document per line, no paragraphs             
            processed_file2_type = 'txt_multiple'
            processed_file2_paragsep = None
        elif this_file2_type==2: # Whole file is one single document, new line is a new paragraph
            processed_file2_type = 'txt_single'
            processed_file2_paragsep = "\n"     
        elif this_file2_type==3: # Whole file is one single document, empty line is a new paragraph            
            processed_file2_type = 'txt_single'
            processed_file2_paragsep = "\n\n"            
        elif this_file2_type==4: # Zipped file with multiple txt files, each is one single document, empty line is a new paragraph 
            raise NotImplementedError("Sorry, this function has not been implemented yet.")        
        else:
            raise ValueError("Unknown file type!")
        
        
        # Load files in object    
        freqanalyzer_obj.load_file(file1_path, group_name="file1", file_type=processed_file1_type, 
                                   encoding=this_file1_encoding, paragraph_sep=processed_file1_paragsep)
        freqanalyzer_obj.load_file(file2_path, group_name="file2", file_type=processed_file2_type, 
                                   encoding=this_file2_encoding, paragraph_sep=processed_file2_paragsep)
    
        # Generate term-document matrix
        freqanalyzer_obj.create_termdocmatrix()
    
        # Get distinctive words
        # Returns a tuple: word, chi-square, pval, word rate (per 1000 words) in group 1, 
        # word rate (per 1000 words) in group 2, group where word appears most
        compact_output = freqanalyzer_obj.get_distinctive_words('file1', 'file2')

        # Format the result for the view        
        keys = ['word', 'chisq', 'pval', 'rate1', 'rate2', 'topgroup']
        formatted_output = [dict(zip(keys,row)) for row in compact_output]
        
        
        #### Generate file for download
        
        # Generate a unique filename
        unique_filename = str(uuid.uuid4())    
        out_filename = os.path.join(config.RESULTS_FOLDER, unique_filename)
        
        sep = ';'
        # Create result file with header
        with io.open(out_filename, 'wt') as f_out: 
            header_line = sep.join(keys)
            f_out.write(header_line+"\n")  
            
        for item in compact_output:
            result_line = sep.join([str(x) for x in item])
            LOG.debug("Result line: %s", result_line)
            # Write the result
            with io.open(out_filename, 'at') as f_out:        
                f_out.write(result_line + "\n")
        

        return formatted_output, unique_filename
    

def cosine_dists(input_filename, filetype, encoding=None, vectorspace_name=None): 
    '''
    Processes a file, considering the filetype
    :param input_filename:
    :param filetype (int):  1: one document per line, no paragraphs; 
    '''
    # First, check if file is not empty
    if os.stat(input_filename).st_size == 0:
        raise ValueError('Empty file')
    else:
        
        LOG.info("Cosine distances analysis request: filename %s; filetype %s, encoding %s", 
                 input_filename, filetype, encoding)
        
        # Create a Tokenizer object
        my_tokenizer_settings = TOKENIZER_SETTINGS
        my_tokenizer_settings.update({"encoding":  encoding})
        tokenizer_obj = LocalTokenizer(**my_tokenizer_settings)
        
        # Load the object with the module for calculating cosine distances
        my_cosdist_settings = COSDIST_SETTINGS
        my_cosdist_settings.update({"tokenizer_obj": tokenizer_obj,
                                    "vectorspace_name": vectorspace_name})
        cosdist_obj = CosineDists(**my_cosdist_settings)
    
                
        # Load feature extractor according to file type
        if filetype==1: # One document per line, no paragraphs   
            
            # Unload previous list, if any
            cosdist_obj.unload_file()
            
            # Read the file
            with io.open(input_filename) as f:
                word_list = f.read().splitlines()
                word_list_processed = [x.replace(" ", "_")[0:20] for x in word_list]
                   
            cosdist_obj.load_list(word_list, doc_index = word_list_processed)
            
            '''elif filetype==2: # Whole file is one single document, new line is a new paragraph            
            ftextr_obj.load_file(input_filename, file_type='txt_single', paragraph_sep="\n", encoding=encoding)
        
        elif filetype==3: # Whole file is one single document, empty line is a new paragraph            
            ftextr_obj.load_file(input_filename, file_type='txt_single', paragraph_sep="\n\n", encoding=encoding)
            
        elif filetype==4: # Zipped file with multiple txt files, each is one single document, empty line is a new paragraph 
            raise NotImplementedError("Sorry, this function has not been implemented yet.")'''
        
        else:
            raise ValueError("Unknown file type!")
            
            
        # Generate a unique filename
        unique_filename = str(uuid.uuid4())    
        out_filename = os.path.join(config.RESULTS_FOLDER, unique_filename)
        
        
        # Create result file with header
        matrix_cosines = cosdist_obj.get_matrix_as_dict()
        LOG.debug(matrix_cosines)
            
        # Convert to dataframe
        matrix_df = pd.DataFrame.from_dict(matrix_cosines)
        LOG.debug(matrix_df.head())
            
        # Write to file
        matrix_df.to_csv(out_filename, sep=b';', encoding='utf-8')

        
        return unique_filename 
    
    
def get_nearest_neighbors(input_string, n=10, vectorspace_name=None):
    '''
    Processes a single text informed by the user.
    
    :param input_string:
    '''
    
    # Create a Tokenizer object
    my_tokenizer_settings = TOKENIZER_SETTINGS
    #my_tokenizer_settings.update({"encoding":  encoding})
    tokenizer_obj = LocalTokenizer(**my_tokenizer_settings)
    
    # Load the object with the module for calculating cosine distances
    my_cosdist_settings = COSDIST_SETTINGS
    my_cosdist_settings.update({"tokenizer_obj": tokenizer_obj,
                                "vectorspace_name": vectorspace_name})
    cosdist_obj = CosineDists(**my_cosdist_settings)

    
    
    #LOG.info("Processing one doc...")    
    if len(input_string)>0:
        
        string_length = len(input_string)
        
        LOG.info("Nearest neighbors analysis request: length %s", string_length)
        
        if string_length <= config.MAX_TEXT_SIZE:
        
            # Get the resulting FeaturesContainer object
            result = cosdist_obj.get_nearest_neighbors(input_string, n=n)
            
            return result
        else:
            raise ValueError("Text too long! Maximum {} characteres.".format(config.MAX_TEXT_SIZE))
    
    else:
        raise ValueError("Empty string")  
