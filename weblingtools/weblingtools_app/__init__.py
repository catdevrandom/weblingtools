# encoding: utf-8
'''
Main module for starting the dashboard application service. Imports required 
modules from Flask (including DB and auth modules), 
and sets up logging.
'''
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import sys
import resource

from flask import Flask
from flask_bootstrap import Bootstrap
from werkzeug.utils import secure_filename

import config



# Try to limit memory usage
def limit_memory_usage(mem_limit):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    if mem_limit:
        mem_limit = int(mem_limit)
        
        if soft != resource.RLIM_INFINITY:
            if mem_limit < soft:
                resource.setrlimit(resource.RLIMIT_AS, (mem_limit, hard))
            else:
                # Somewhere else a lower memory limit is already set, let's stick to that
                pass
        else:
            #No soft limit was set before, let's set it now
            resource.setrlimit(resource.RLIMIT_AS, (mem_limit, hard))


def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)



def create_app():
    # We are using the "Application Factory"-pattern here:
    # http://flask.pocoo.org/docs/patterns/appfactories/

    app = Flask(__name__)
    Bootstrap(app)
    
    # Our application uses blueprints. Import and register the blueprint:
    from .views import weblingtools_blueprint
    app.register_blueprint(weblingtools_blueprint)
    
    
    # Initialize navigation
    from .nav import nav
    nav.init_app(app)
    

    # Configure app
    app.config.from_object('config')
    
    # Debug?
    debug_setting = app.config['DEBUG_SETTING']
    

    # Logging:
    import logging
    from logging.handlers import RotatingFileHandler
    #from logging import StreamHandler

    # Add logging for the lingtools library
    #lingtoolslogger = logging.getLogger('lingtools')
    
    if app.logger is None:
        #app.logger = logging.getLogger(__name__)
        app.logger = logging.getLogger()
    
    # Debug vs non-debug settings
    if debug_setting:
        app.logger.setLevel(logging.DEBUG)
        #lingtoolslogger.setLevel(logging.DEBUG)
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['TEMPLATES_AUTO_RELOAD'] = True
        logging_file = app.config['LOG_FILENAME_TEST']    
    else:
        app.logger.setLevel(logging.INFO)
        #lingtoolslogger.setLevel(logging.INFO)
        app.config['TESTING'] = False
        app.config['WTF_CSRF_ENABLED'] = True
        app.config['TEMPLATES_AUTO_RELOAD'] = False
        logging_file = app.config['LOG_FILENAME']
        
    file_max_bytes = 1 * 1000 * 1000 #1MB
    file_handler = RotatingFileHandler(os.path.join(app.config['TMPDIR'], logging_file), 'a', maxBytes = file_max_bytes , backupCount = 10)
    logformat = logging.Formatter('%(asctime)s %(levelname)s [%(filename)s:%(lineno)d]: %(message)s')
    file_handler.setFormatter(logformat)
    
    if debug_setting:
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler.setLevel(logging.INFO)    


    # Add file handler to flask app logger and to lingtoolslogger
    app.logger.addHandler(file_handler)
    #lingtoolslogger.addHandler(file_handler)
  

    app.logger.info('Weblingtools Start Up (Debug = %s)...', debug_setting)
    
    
    # Set memory limit?
    try:
        server_memory_limit = app.config['MEMORY_LIMIT']
    except KeyError:
        server_memory_limit = None
    
    if server_memory_limit:
        limit_memory_usage(server_memory_limit)
        app.logger.info("Limiting soft and hard memory consumption to: {}".format(sizeof_fmt(server_memory_limit)))
    else:
        soft, hard = resource.getrlimit(resource.RLIMIT_AS)
        if hard == resource.RLIM_INFINITY and soft == resource.RLIM_INFINITY :
            app.logger.warning("There are no limits for memory usage! Very large files may crash the server")
        else:
            app.logger.info("Memory limit has not been defined. Using default soft/hard limits: {}/{}".format(
            sizeof_fmt(soft),
            sizeof_fmt(hard)))

    return app
