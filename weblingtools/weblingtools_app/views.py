# encoding: utf-8
'''
Created on 12 apr. 2017

@author: brandaoc

Defines the methods/endpoints (the views) of the userprofile service.
This defines the interaction points, but the actual logic is treated
by the :mod:`controller`.
'''
from __future__ import unicode_literals
from __future__ import absolute_import

import os

import warnings

#Flask and modules
from flask import Blueprint
from flask import render_template
from flask import request
from flask import send_file
from werkzeug.utils import secure_filename

from lingtools.util import get_vectorspaces


import errno

import config 

#Exceptions and errors
#from flask.ext.api.exceptions import AuthenticationFailed, ParseError
#from sqlalchemy.orm.exc import NoResultFound
#from .errors import *

#Python modules
#import datetime
#import json
#import time
#import simplejson

#Import controller
from weblingtools_app import controller

#Extensions
#from .extensions import LOG

#Logging
import logging
LOG = logging.getLogger(__name__)

# blueprint
weblingtools_blueprint = Blueprint('weblingtools_blueprint', __name__, url_prefix='')
#error_pages = Blueprint('error_pages', __name__)

# Filename treatment
def allowed_file(fileobj):
    filename = fileobj.filename
    mimetype = fileobj.mimetype
    if ('.' in filename) and (filename.rsplit('.', 1)[1].lower() in config.ALLOWED_EXTENSIONS) and (mimetype in config.ALLOWED_MIMETYPES):
        return True
    else:
        return False  


################################

@weblingtools_blueprint.route('/')
def index(name=None):
    '''
    Says hello.
    '''
    #return 'Hello world!'
    return render_template('index.html', name=name)

@weblingtools_blueprint.route('/help')
def helppage(name=None):
    '''
    Help page.
    '''
    #return 'Hello world!'
    return render_template('help.html', name=name)



@weblingtools_blueprint.route('/simple', methods=['POST', 'GET'])
def process_simple(name=None):
    '''
    Processes the text and shows results.
    '''
    errormsg = None
    warningmsg = None
    
    # Get available vector spaces
    vec_spaces = get_vectorspaces()

    if request.method == 'POST':
        input_text = request.form['text']
        vectorspace_name = request.form['vectorspace_name']

        # Clean up new lines in incoming string
        input_text = "\n".join(input_text.splitlines())
         
        
        # Process text
        result = controller.process_text(input_text, vectorspace_name=vectorspace_name)
        #LOG.debug("The result that will be sent to the view: %r", result) 
        
        # generate file for download
        #file_for_download = 'helloworld.txt'
        
        # Return result
        #return render_template('results.html', errormsg=errormsg, result = result, text = input_text, file_for_download=file_for_download)
        return render_template('results.html', errormsg=errormsg, result = result, text = input_text)
    
    else:
        return render_template('simple.html', 
                               name=name, 
                               errormsg=errormsg, 
                               warningmsg=warningmsg, 
                               max_text_size=config.MAX_TEXT_SIZE,
                               vectorspaces=vec_spaces)
    


@weblingtools_blueprint.route('/file', methods=['GET', 'POST'])
def process_file(name=None):
    '''  
    Page that receives a text file for analysis of several texts at once.
    '''
    errormsg = None
    warningmsg = None
    
    # Get available vector spaces
    vec_spaces = get_vectorspaces()
    
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            errormsg = 'Bad request: no file'
            return render_template('error.html', errormsg=errormsg), 400
        myfile = request.files['file']
        vectorspace_name = request.form['vectorspace_name']
        
        # if user does not select file, browser also
        # submit a empty part without filename
        if myfile.filename == '':
            errormsg = 'Bad request: no selected file'
            return render_template('error.html', errormsg=errormsg), 400
        if myfile and allowed_file(myfile): 

            
            # Save file locally
            uploaded_filename = os.path.join(config.UPLOAD_FOLDER, secure_filename(myfile.filename))
            myfile.save(uploaded_filename)
            LOG.info('File uploaded: %s', uploaded_filename)
            
            # Get file type
            filetype = request.form['fileformat']
            LOG.info("Filetype: {}".format(filetype))
            if int(filetype) in [1,2,3,4]:
                filetype = int(filetype)
            else:
                filetype = 1 # Default is one document per line
            
            # Process file and get filename of processed csv
            resulting_file = controller.process_file(uploaded_filename, filetype, vectorspace_name=vectorspace_name)
            
            # Return the results
            return render_template('results_file.html', name=name, file_for_download=resulting_file)

                
        else:
            fileextension = myfile.filename.split('.')[-1]
            errormsg = 'File type not allowed'
            LOG.warning( 'User tried to send file not allowed: %s', fileextension)
            return render_template('error.html', errormsg=errormsg), 400
            
    else:
        max_file_size = config.MAX_CONTENT_LENGTH
        return render_template('file.html', 
                               name=name, 
                               max_file_size=max_file_size, 
                               errormsg=errormsg, 
                               warningmsg=warningmsg,
                               vectorspaces=vec_spaces)

@weblingtools_blueprint.route('/download/<filename>')
def download_file(filename=None):
    if filename:
        LOG.info( "Serving file: %s", filename)
        try:
            return send_file(os.path.join(config.RESULTS_FOLDER, filename), as_attachment=True, mimetype="text/csv", attachment_filename="results.csv")
        except EnvironmentError as e:
            LOG.error(e)
            if e.errno == errno.ENOENT:
                errormsg = "File does not exist or expired"
                return render_template('error.html', errormsg=errormsg), 404
            else:
                raise e
    else:
        errormsg = 'Invalid request'
        return render_template('file.html', errormsg=errormsg), 400

 
@weblingtools_blueprint.route('/dynamicthemes', methods=['GET', 'POST'])
def dynamic_themes(name=None):
    errormsg = None
    warningmsg = None
    
    # Get available vector spaces
    vec_spaces = get_vectorspaces()
    
    if request.method == 'POST':
        
        theme_name = request.form['theme_name']
        theme_words = request.form['theme_words']
        vectorspace_name = request.form['vectorspace_name']
        #input_text = request.form['text']
        
        # Split theme words
        theme_words = theme_words.replace(',', "\n")
        theme_words = theme_words.splitlines()
        
        # check if the post request has the file part
        if 'file' not in request.files:
            errormsg = 'Bad request: no file'
            return render_template('error.html', errormsg=errormsg), 400
        myfile = request.files['file']
        
        # if user does not select file, browser also
        # submit a empty part without filename
        if myfile.filename == '':
            errormsg = 'Bad request: no selected file'
            return render_template('error.html', errormsg=errormsg), 400
        if myfile and allowed_file(myfile):
            
            # Save file locally
            uploaded_filename = os.path.join(config.UPLOAD_FOLDER, secure_filename(myfile.filename))
            myfile.save(uploaded_filename)
            LOG.info('File uploaded: %s', uploaded_filename)
            
            # Get file type
            filetype = request.form['fileformat']
            LOG.info("Filetype: {}".format(filetype))
            if int(filetype) in [1,2,3,4]:
                filetype = int(filetype)
            else:
                filetype = 1 # Default is one document per line
                
            # Get encoding
            encoding = request.form['encoding']
            
            warningmsgs = []
            
            with warnings.catch_warnings(record=True) as all_warnings:
                # Cause all warnings to always be triggered
                warnings.simplefilter("always")
                
                
                # Process file and get filename of processed csv
                resulting_file = controller.process_dynamic_themes(theme_name, 
                                                                   theme_words, 
                                                                   uploaded_filename, 
                                                                   filetype, 
                                                                   encoding=encoding,
                                                                   vectorspace_name=vectorspace_name)
                
                # ignore any non-custom warnings that may be in the list
                #w = filter(lambda i: issubclass(i.category, UserWarning), w)
    
    
                if len(all_warnings):
                    for w in all_warnings:
                        warningmsgs.append(str(w.message))
                    
                if len(warningmsgs)>0:
                    LOG.warning(warningmsgs)
            
            if len(warningmsgs)>0:
                warningmsg = '. '.join(warningmsgs)
            
            # Return the results
            return render_template('results_file.html', name=name, file_for_download=resulting_file, errormsg=errormsg, warningmsg=warningmsg)
        else:
            fileextension = myfile.filename.split('.')[-1]
            errormsg= 'File type not allowed'
            LOG.warning( 'User tried to send file not allowed: %s', fileextension)
            return render_template('error.html', errormsg=errormsg), 400
            
    else:
        max_file_size = config.MAX_CONTENT_LENGTH
        max_number_theme_words = config.MAX_NUMBER_THEME_WORDS
        return render_template('dynamicthemes.html', 
                               name=name, 
                               errormsg=errormsg, 
                               warningmsg=warningmsg, 
                               max_file_size=max_file_size, 
                               max_number_theme_words = max_number_theme_words,
                               vectorspaces=vec_spaces)





@weblingtools_blueprint.route('/distinctivewords', methods=['GET', 'POST'])
def distinctive_words(name=None):
    
    errormsg = None
    warningmsg = None

    
    if request.method == 'POST':
        
        # check if the post request has the file part
        if 'file1' and 'file2' not in request.files:
            errormsg = 'Bad request: no file'
            return render_template('error.html', errormsg=errormsg), 400

        # Get the form values
        myfile1 = request.files['file1']
        file1_encoding = request.form['file2encoding']
        file1_type = request.form['file1format']
        myfile2 = request.files['file2']
        file2_encoding = request.form['file2encoding']
        file2_type = request.form['file2format']
                
        # if user does not select file, browser also
        # submit a empty part without filename
        if myfile1.filename == '' or myfile2.filename == '':
            errormsg = 'Bad request: no selected file'
            return render_template('error.html', errormsg=errormsg), 400
        
        if myfile1 and myfile2 and allowed_file(myfile1) and allowed_file(myfile2):
            LOG.warning("Files exist and are allowed")
            # Save file locally
            uploaded_filename1 = os.path.join(config.UPLOAD_FOLDER, secure_filename(myfile1.filename))
            uploaded_filename2 = os.path.join(config.UPLOAD_FOLDER, secure_filename(myfile2.filename))
            myfile1.save(uploaded_filename1)
            myfile2.save(uploaded_filename2)
            LOG.info('Files uploaded: %s and %s', uploaded_filename1, uploaded_filename2)
            
            # Get file type
            LOG.debug("Filetype (file 1): {}".format(file1_type))
            LOG.debug("Filetype (file 2): {}".format(file2_type))
            if int(file1_type) in [1,2,3,4]:
                file1_type = int(file1_type)
            else:
                file1_type = 1 # Default is one document per line
                
            if int(file2_type) in [1,2,3,4]:
                file2_type = int(file2_type)
            else:
                file2_type = 1 # Default is one document per line
                
            warningmsgs = []
            
            with warnings.catch_warnings(record=True) as all_warnings:
                # Cause all warnings to always be triggered
                warnings.simplefilter("always")
                
                
                # Process file, get results for display and filename for download
                result, file_for_download = controller.get_distinctive_words(uploaded_filename1, uploaded_filename2, 
                                              file1_encoding=file1_encoding, file2_encoding=file2_encoding,
                                              file1_type = file1_type, file2_type = file2_type)
                
                # ignore any non-custom warnings that may be in the list
                #w = filter(lambda i: issubclass(i.category, UserWarning), w)
    
    
                if len(all_warnings):
                    for w in all_warnings:
                        warningmsgs.append(str(w.message))
                    
                LOG.debug("Warning messages: %s", warningmsgs)
            
            if len(warningmsgs)>0:
                warningmsg = '. '.join(warningmsgs)
            
            
            
            # Create a key for row color in the result
            formatted_result = []
            color1 = '#BCC6CC'
            color2 = '#FFF8DC'
            for item in result:
                newitem = item
                newitem['color'] = color1 if item['topgroup'] == 1 else color2
                formatted_result.append(newitem)
            
            LOG.debug("Formatted result: %s", formatted_result)
            
            # Return the results
            return render_template('distinctivewords_results.html', name=name, result=formatted_result, 
                                   file_for_download=file_for_download, errormsg=errormsg, warningmsg=warningmsg, 
                                   color1=color1, color2=color2)
        else:
            errormsg= 'File type not allowed'
            LOG.warning( 'User tried to send file with extension that is not allowed')
            return render_template('error.html', errormsg=errormsg), 400
            
    else:
        max_file_size = config.MAX_CONTENT_LENGTH
        return render_template('distinctivewords.html', 
                               name=name, 
                               errormsg=errormsg, 
                               warningmsg=warningmsg, 
                               max_file_size=max_file_size)



@weblingtools_blueprint.route('/cosinedist', methods=['GET', 'POST'])
def cosine_distances(name=None):
    
    errormsg = None
    warningmsg = None
    
    # Get available vector spaces
    vec_spaces = get_vectorspaces()
    
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            errormsg = 'Bad request: no file'
            return render_template('error.html', errormsg=errormsg), 400
        myfile = request.files['file']
        vectorspace_name = request.form['vectorspace_name']
        
        # if user does not select file, browser also
        # submit a empty part without filename
        if myfile.filename == '':
            errormsg = 'Bad request: no selected file'
            return render_template('error.html', errormsg=errormsg), 400
        if myfile and allowed_file(myfile): 

            
            # Save file locally
            uploaded_filename = os.path.join(config.UPLOAD_FOLDER, secure_filename(myfile.filename))
            myfile.save(uploaded_filename)
            LOG.info('File uploaded: %s', uploaded_filename)
            
            # Get file type
            filetype = request.form['fileformat']
            LOG.info("Filetype: {}".format(filetype))
            if int(filetype) in [1,2,3,4]:
                filetype = int(filetype)
            else:
                filetype = 1 # Default is one document per line
            
            # Process file and get filename of processed csv
            resulting_file = controller.cosine_dists(uploaded_filename, filetype, vectorspace_name=vectorspace_name)
            
            # Return the results
            return render_template('results_file.html', name=name, file_for_download=resulting_file)

                
        else:
            fileextension = myfile.filename.split('.')[-1]
            errormsg = 'File type not allowed'
            LOG.warning( 'User tried to send file not allowed: %s', fileextension)
            return render_template('error.html', errormsg=errormsg), 400
            
    else:
        max_file_size = config.MAX_CONTENT_LENGTH
        return render_template('cosinedist.html', 
                               name=name, 
                               max_file_size=max_file_size, 
                               errormsg=errormsg, 
                               warningmsg=warningmsg,
                               vectorspaces=vec_spaces)


@weblingtools_blueprint.route('/neighbors', methods=['POST', 'GET'])
def get_nearest_neighbors(name=None):
    '''
    Gets nearest neighbors from a word
    '''
    errormsg = None
    warningmsg = None
    
    # Get available vector spaces
    vec_spaces = get_vectorspaces()

    if request.method == 'POST':
        input_text = request.form['text'] 
        n = request.form["n"]
        vectorspace_name = request.form['vectorspace_name']
        
        try:
            n = int(n)
        except Exception:
            n=10
        
        
        n=n+1
        # Clean up new lines in incoming string
        input_text = "\n".join(input_text.splitlines())         
        
        # Process text
        result = controller.get_nearest_neighbors(input_text, n=n, vectorspace_name=vectorspace_name)
        #LOG.debug("The result that will be sent to the view: %r", result) 
        
        # Return result
        return render_template('results_neighbors.html', errormsg=errormsg, result = list(enumerate(result)), text = input_text)
    
    else:
        return render_template('neighbors.html', 
                               name=name, 
                               errormsg=errormsg, 
                               warningmsg=warningmsg, 
                               max_text_size=config.MAX_TEXT_SIZE,
                               vectorspaces=vec_spaces)
    



####################################
## Errors

@weblingtools_blueprint.errorhandler(413)
def request_entity_too_large(e):
    LOG.warning( 'User tried to send file larger than maximum allowed: %s',  e)
    return render_template('error.html', errormsg='Error 413: File too big'), 413

@weblingtools_blueprint.errorhandler(ValueError)
def value_error(e):
    #LOG.error(e, exc_info=True)
    LOG.error(e, exc_info=True)
    errormsg = "There has been a problem processing your request. Please check your input"
    return render_template('error.html', errormsg=errormsg), 400

@weblingtools_blueprint.errorhandler(MemoryError)
def memory_error(e):
    #LOG.error(e, exc_info=True)
    LOG.error(e, exc_info=True)
    errormsg = "Could not process your file. Try sending a smaller file, dividing file into multiple documents, or selecting a smaller vector space"
    return render_template('error.html', errormsg=errormsg), 400

@weblingtools_blueprint.errorhandler(RuntimeError)
def runtime_error(e):
    #LOG.error(e, exc_info=True)
    LOG.error(e, exc_info=True)
    errormsg = "There has been a problem processing your request. Please contact the system admin"
    return render_template('error.html', errormsg=errormsg), 400

@weblingtools_blueprint.errorhandler(Exception)
def unhandled_exception(e):
    LOG.error(e, exc_info=True)
    return render_template('error.html', errormsg='Unhandled exception. A message has been sent to the system admin'), 500

