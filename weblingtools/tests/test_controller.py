# -*- coding: utf-8 -*-
'''
Created on 18 apr. 2017

@author: brandaoc
'''
from __future__ import unicode_literals
from __future__ import absolute_import

import pytest
import io
import os

from weblingtools_app import controller


import logging
LOG = logging.getLogger(__name__)
LOG_LINGTOOLS = logging.getLogger('lingtools')

##################################################
# Expected result
##################################################

EXPECTED_RESULT = {u'feature_sets_with_ids': [{u'code': u'basic', u'features': [{u'code':
u'word_count', u'name': u'word_count', u'value': 14, u'id': 1}, {u'code':
u'sentence_count', u'name': u'sentence_count', u'value': 2, u'id': 2}, {u'code':
u'paragraph_count', u'name': u'paragraph_count', u'value': 1, u'id': 3},
{u'code': u'avg_word_length', u'name': u'avg_word_length', u'value':
3.2857142857142856, u'id': 4}, {u'code': u'sd_word_length', u'name':
u'sd_word_length', u'value': 1.3850513878332369, u'id': 5}, {u'code':
u'avg_sentence_length', u'name': u'avg_sentence_length', u'value': 8.0, u'id':
6}, {u'code': u'sd_sentence_length', u'name': u'sd_sentence_length', u'value':
5.0, u'id': 7}, {u'code': u'avg_paragraph_length', u'name':
u'avg_paragraph_length', u'value': 2.0, u'id': 8}, {u'code':
u'sd_paragraph_length', u'name': u'sd_paragraph_length', u'value': 0.0, u'id':
9}, {u'code': u'noun_incidence', u'name': u'noun_incidence', u'value':
0.2857142857142857, u'id': 10}, {u'code': u'verb_incidence', u'name':
u'verb_incidence', u'value': 0.21428571428571427, u'id': 11}, {u'code':
u'adjective_incidence', u'name': u'adjective_incidence', u'value':
0.07142857142857142, u'id': 12}, {u'code': u'adverb_incidence', u'name':
u'adverb_incidence', u'value': 0.14285714285714285, u'id': 13}, {u'code':
u'pronoun_incidence', u'name': u'pronoun_incidence', u'value':
0.07142857142857142, u'id': 14}, {u'code': u'1p_sing_pronoun_incidence',
u'name': u'1p_sing_pronoun_incidence', u'value': 0.14285714285714285, u'id':
15}, {u'code': u'1p_pl_pronoun_incidence', u'name': u'1p_pl_pronoun_incidence',
u'value': 0.0, u'id': 16}, {u'code': u'2p_pronoun_incidence', u'name':
u'2p_pronoun_incidence', u'value': 0.0, u'id': 17}, {u'code':
u'3p_sing_pronoun_incidence', u'name': u'3p_sing_pronoun_incidence', u'value':
0.0, u'id': 18}, {u'code': u'3p_pl_pronoun_incidence', u'name':
u'3p_pl_pronoun_incidence', u'value': 0.0, u'id': 19}], u'id': 1, u'name':
u'Basic'}, {u'code': u'biber', u'features': [{u'code': u'rule1_past_tense',
u'name': u'rule1_past_tense', u'value': 0.0, u'id': 20}, {u'code':
u'rule2_perfect_aspect_verbs', u'name': u'rule2_perfect_aspect_verbs', u'value':
0.0, u'id': 21}, {u'code': u'rule3_present_tense', u'name':
u'rule3_present_tense', u'value': 125.0, u'id': 22}, {u'code':
u'rule4_place_adverbials', u'name': u'rule4_place_adverbials', u'value': 0.0,
u'id': 23}, {u'code': u'rule5_time_adverbials', u'name':
u'rule5_time_adverbials', u'value': 0.0, u'id': 24}, {u'code':
u'rule6_first_person_pronouns', u'name': u'rule6_first_person_pronouns',
u'value': 125.0, u'id': 25}, {u'code': u'rule7_second_person_pronouns', u'name':
u'rule7_second_person_pronouns', u'value': 0.0, u'id': 26}, {u'code':
u'rule8_third_person_pronouns', u'name': u'rule8_third_person_pronouns',
u'value': 0.0, u'id': 27}, {u'code': u'rule9_pronoun_it', u'name':
u'rule9_pronoun_it', u'value': 0.0, u'id': 28}, {u'code':
u'rule10_demonstrative_pronouns', u'name': u'rule10_demonstrative_pronouns',
u'value': 0.0, u'id': 29}, {u'code': u'rule11_indefinite_pronouns', u'name':
u'rule11_indefinite_pronouns', u'value': 0.0, u'id': 30}, {u'code':
u'rule12_do_as_proverb', u'name': u'rule12_do_as_proverb', u'value': 0.0, u'id':
31}, {u'code': u'rule13_wh_questions', u'name': u'rule13_wh_questions',
u'value': 0.0, u'id': 32}, {u'code': u'rule14_nominalizations', u'name':
u'rule14_nominalizations', u'value': 0.0, u'id': 33}, {u'code':
u'rule15_gerunds', u'name': u'rule15_gerunds', u'value': 0.0, u'id': 34},
{u'code': u'rule16_nouns', u'name': u'rule16_nouns', u'value': 250.0, u'id':
35}, {u'code': u'rule17_agentless_passives', u'name':
u'rule17_agentless_passives', u'value': 0.0, u'id': 36}, {u'code':
u'rule18_by_passives', u'name': u'rule18_by_passives', u'value': 0.0, u'id':
37}, {u'code': u'rule19_be_as_main_verb', u'name': u'rule19_be_as_main_verb',
u'value': 0.0, u'id': 38}, {u'code': u'rule20_existential_there', u'name':
u'rule20_existential_there', u'value': 0.0, u'id': 39}, {u'code':
u'rule21_that_verb_complements', u'name': u'rule21_that_verb_complements',
u'value': 0.0, u'id': 40}, {u'code': u'rule22_that_adj_complements', u'name':
u'rule22_that_adj_complements', u'value': 0.0, u'id': 41}, {u'code':
u'rule23_wh_clauses', u'name': u'rule23_wh_clauses', u'value': 0.0, u'id': 42},
{u'code': u'rule24_infinitives', u'name': u'rule24_infinitives', u'value': 62.5,
u'id': 43}, {u'code': u'rule25_present_participial_clauses', u'name':
u'rule25_present_participial_clauses', u'value': 0.0, u'id': 44}, {u'code':
u'rule26_past_participial_clauses', u'name': u'rule26_past_participial_clauses',
u'value': 0.0, u'id': 45}, {u'code': u'rule27_past_prt_whiz_deletions', u'name':
u'rule27_past_prt_whiz_deletions', u'value': 0.0, u'id': 46}, {u'code':
u'rule28_present_prt_whiz_deletions', u'name':
u'rule28_present_prt_whiz_deletions', u'value': 0.0, u'id': 47}, {u'code':
u'rule29_that_relatives_subj_position', u'name':
u'rule29_that_relatives_subj_position', u'value': 0.0, u'id': 48}, {u'code':
u'rule30_that_relatives_obj_position', u'name':
u'rule30_that_relatives_obj_position', u'value': 0.0, u'id': 49}, {u'code':
u'rule31_wh_relatives_subj_position', u'name':
u'rule31_wh_relatives_subj_position', u'value': 0.0, u'id': 50}, {u'code':
u'rule32_wh_relatives_obj_position', u'name':
u'rule32_wh_relatives_obj_position', u'value': 0.0, u'id': 51}, {u'code':
u'rule33_wh_relatives_pied_pipes', u'name': u'rule33_wh_relatives_pied_pipes',
u'value': 0.0, u'id': 52}, {u'code': u'rule34_sentence_relatives', u'name':
u'rule34_sentence_relatives', u'value': 0.0, u'id': 53}, {u'code':
u'rule35_adv_subordinator_cause', u'name': u'rule35_adv_subordinator_cause',
u'value': 0.0, u'id': 54}, {u'code': u'rule36_adv_sub_concesssion', u'name':
u'rule36_adv_sub_concesssion', u'value': 0.0, u'id': 55}, {u'code':
u'rule37_adv_sub_condition', u'name': u'rule37_adv_sub_condition', u'value':
0.0, u'id': 56}, {u'code': u'rule38_adv_sub_other', u'name':
u'rule38_adv_sub_other', u'value': 0.0, u'id': 57}, {u'code':
u'rule39_prepositions', u'name': u'rule39_prepositions', u'value': 62.5, u'id':
58}, {u'code': u'rule40_attributive_adjectives', u'name':
u'rule40_attributive_adjectives', u'value': 62.5, u'id': 59}, {u'code':
u'rule41_predicative_adjectives', u'name': u'rule41_predicative_adjectives',
u'value': 0.0, u'id': 60}, {u'code': u'rule42_adverbs', u'name':
u'rule42_adverbs', u'value': 62.5, u'id': 61}, {u'code':
u'rule43_type_token_ratio', u'name': u'rule43_type_token_ratio', u'value': 0.0,
u'id': 62}, {u'code': u'rule44_word_length', u'name': u'rule44_word_length',
u'value': 3.0, u'id': 63}, {u'code': u'rule45_conjuncts', u'name':
u'rule45_conjuncts', u'value': 0.0, u'id': 64}, {u'code': u'rule46_downtoners',
u'name': u'rule46_downtoners', u'value': 0.0, u'id': 65}, {u'code':
u'rule47_hedges', u'name': u'rule47_hedges', u'value': 0.0, u'id': 66},
{u'code': u'rule48_amplifiers', u'name': u'rule48_amplifiers', u'value': 62.5,
u'id': 67}, {u'code': u'rule49_empathics', u'name': u'rule49_empathics',
u'value': 0.0, u'id': 68}, {u'code': u'rule50_discourse_particles', u'name':
u'rule50_discourse_particles', u'value': 0.0, u'id': 69}, {u'code':
u'rule51_demonstratives', u'name': u'rule51_demonstratives', u'value': 0.0,
u'id': 70}, {u'code': u'rule52_possibility_modals', u'name':
u'rule52_possibility_modals', u'value': 0.0, u'id': 71}, {u'code':
u'rule53_necessity_modals', u'name': u'rule53_necessity_modals', u'value': 0.0,
u'id': 72}, {u'code': u'rule54_predictive_modals', u'name':
u'rule54_predictive_modals', u'value': 0.0, u'id': 73}, {u'code':
u'rule55_public_verbs', u'name': u'rule55_public_verbs', u'value': 0.0, u'id':
74}, {u'code': u'rule56_private_verbs', u'name': u'rule56_private_verbs',
u'value': 0.0, u'id': 75}, {u'code': u'rule57_suasive_verbs', u'name':
u'rule57_suasive_verbs', u'value': 0.0, u'id': 76}, {u'code':
u'rule58_seems_appear', u'name': u'rule58_seems_appear', u'value': 0.0, u'id':
77}, {u'code': u'rule59_contractions', u'name': u'rule59_contractions',
u'value': 62.5, u'id': 78}, {u'code': u'rule60_that_deletion', u'name':
u'rule60_that_deletion', u'value': 0.0, u'id': 79}, {u'code':
u'rule61_stranded_prepositions', u'name': u'rule61_stranded_prepositions',
u'value': 0.0, u'id': 80}, {u'code': u'rule62_split_infinitives', u'name':
u'rule62_split_infinitives', u'value': 0.0, u'id': 81}, {u'code':
u'rule63_split_auxilaries', u'name': u'rule63_split_auxilaries', u'value': 0.0,
u'id': 82}, {u'code': u'rule64_phrasal_coordination', u'name':
u'rule64_phrasal_coordination', u'value': 0.0, u'id': 83}, {u'code':
u'rule65_non_phrasal_coordination', u'name': u'rule65_non_phrasal_coordination',
u'value': 0.0, u'id': 84}, {u'code': u'rule66_synthetic_negation', u'name':
u'rule66_synthetic_negation', u'value': 0.0, u'id': 85}, {u'code':
u'rule67_analytic_negation', u'name': u'rule67_analytic_negation', u'value':
0.0, u'id': 86}], u'id': 2, u'name': u'Biber'}, {u'code': u'mrc', u'features':
[{u'code': u'avg_Nlet', u'name': u'avg_Nlet', u'value': 3.3636363636363638,
u'id': 87}, {u'code': u'sd_Nlet', u'name': u'sd_Nlet', u'value':
1.3666633071248098, u'id': 88}, {u'code': u'avg_Nphon', u'name': u'avg_Nphon',
u'value': 2.4545454545454546, u'id': 89}, {u'code': u'sd_Nphon', u'name':
u'sd_Nphon', u'value': 1.2331509060227761, u'id': 90}, {u'code': u'avg_Nsyl',
u'name': u'avg_Nsyl', u'value': 1.2727272727272727, u'id': 91}, {u'code':
u'sd_Nsyl', u'name': u'sd_Nsyl', u'value': 0.44536177141512329, u'id': 92},
{u'code': u'avg_K-F-freq', u'name': u'avg_K-F-freq', u'value':
7216.818181818182, u'id': 93}, {u'code': u'sd_K-F-freq', u'name':
u'sd_K-F-freq', u'value': 10066.273471513963, u'id': 94}, {u'code':
u'avg_K-F-ncats', u'name': u'avg_K-F-ncats', u'value': 14.272727272727273,
u'id': 95}, {u'code': u'sd_K-F-ncats', u'name': u'sd_K-F-ncats', u'value':
2.0041279713680549, u'id': 96}, {u'code': u'avg_K-F-nsamp', u'name':
u'avg_K-F-nsamp', u'value': 317.09090909090907, u'id': 97}, {u'code':
u'sd_K-F-nsamp', u'name': u'sd_K-F-nsamp', u'value': 168.81590540394993, u'id':
98}, {u'code': u'avg_T-L-freq', u'name': u'avg_T-L-freq', u'value':
29272.18181818182, u'id': 99}, {u'code': u'sd_T-L-freq', u'name':
u'sd_T-L-freq', u'value': 47353.493838303148, u'id': 100}, {u'code': 
u'avg_Brown-freq', u'name': u'avg_Brown-freq', u'value': 1932.2727272727273,
u'id': 101}, {u'code': u'sd_Brown-freq', u'name': u'sd_Brown-freq', u'value':
2358.8201245888354, u'id': 102}, {u'code': u'avg_Familiarity', u'name':
u'avg_Familiarity', u'value': 555.18181818181813, u'id': 103}, {u'code':
u'sd_Familiarity', u'name': u'sd_Familiarity', u'value': 176.17440644884633,
u'id': 104}, {u'code': u'avg_Concreteness', u'name': u'avg_Concreteness',
u'value': 286.36363636363637, u'id': 105}, {u'code': u'sd_Concreteness',
u'name': u'sd_Concreteness', u'value': 142.5822076923107, u'id': 106}, {u'code':
u'avg_Imageability', u'name': u'avg_Imageability', u'value': 316.54545454545456,
u'id': 107}, {u'code': u'sd_Imageability', u'name': u'sd_Imageability',
u'value': 159.01535868437571, u'id': 108}, {u'code': u'avg_Meaningfulness-Colorado', 
u'name': u'avg_Meaningfulness-Colorado', u'value':
354.27272727272725, u'id': 109}, {u'code': u'sd_Meaningfulness-Colorado',
u'name': u'sd_Meaningfulness-Colorado', u'value': 157.93789971152984, u'id':
110}, {u'code': u'avg_Meaningfulness-Paivio', u'name': 
u'avg_Meaningfulness-Paivio', u'value': 62.545454545454547, u'id': 111}, {u'code': 
u'sd_Meaningfulness-Paivio', u'name': u'sd_Meaningfulness-Paivio', u'value':
197.78609365416773, u'id': 112}, {u'code': u'avg_Age-of-acquisition', 
u'name': u'avg_Age-of-acquisition', u'value': 0.0, u'id': 113}, 
{u'code': u'sd_Age-of-acquisition', u'name': u'sd_Age-of-acquisition', u'value': 0.0, u'id': 114}],
u'id': 3, u'name': u'MRC'}, {u'code': u'anew', u'features': [{u'code':
u'avg_valence', u'name': u'avg_valence', u'value': 6.6825000047683716, u'id':
115}, {u'code': u'sd_valence', u'name': u'sd_valence', u'value':
1.075415774637807, u'id': 116}, {u'code': u'avg_arousal', u'name':
u'avg_arousal', u'value': 4.267500102519989, u'id': 117}, {u'code':
u'sd_arousal', u'name': u'sd_arousal', u'value': 1.1688964635038817, u'id':
118}, {u'code': u'avg_dominance', u'name': u'avg_dominance', u'value':
5.7424999475479126, u'id': 119}, {u'code': u'sd_dominance', u'name':
u'sd_dominance', u'value': 0.86065025058457778, u'id': 120}], u'id': 4, u'name':
u'ANEW'}, {u'code': u'semanticvectors', u'features': [{u'code':
u'avg_cosdis_adjacent_sentences', u'name': u'avg_cosdis_adjacent_sentences',
u'value': 0.012430603383869213, u'id': 121}, {u'code':
u'sd_cosdis_adjacent_sentences', u'name': u'sd_cosdis_adjacent_sentences',
u'value': 0.0, u'id': 122}, {u'code': u'avg_cosdis_all_sentences_in_paragraph',
u'name': u'avg_cosdis_all_sentences_in_paragraph', u'value':
0.012430603383869213, u'id': 123}, {u'code':
u'sd_cosdis_all_sentences_in_paragraph', u'name':
u'sd_cosdis_all_sentences_in_paragraph', u'value': 0.0, u'id': 124}, {u'code':
u'avg_cosdis_adjacent_paragraphs', u'name': u'avg_cosdis_adjacent_paragraphs',
u'value': None, u'id': 125}, {u'code': u'sd_cosdis_adjacent_paragraphs', u'name':
u'sd_cosdis_adjacent_paragraphs', u'value': None, u'id': 126}, {u'code':
u'avg_givenness_sentences', u'name': u'avg_givenness_sentences', u'value':
0.012430603383869213, u'id': 127}, {u'code': u'sd_givenness_sentences',
u'name': u'sd_givenness_sentences', u'value': 0.0, u'id': 128}], u'id': 5,
u'name': u'SemanticVectors'}, {u'code': u'sentimentanalysisfeatures',
u'features': [{u'code': u'positive', u'name': u'positive', u'value': 0.1425,
u'id': 129}, {u'code': u'negative', u'name': u'negative', u'value': 0.0, u'id':
130}, {u'code': u'neutral', u'name': u'neutral', u'value': 0.8574999999999999,
u'id': 131}, {u'code': u'compound', u'name': u'compound', u'value': 0.30575,
u'id': 132}], u'id': 6, u'name': u'SentimentAnalysisFeatures'}, {u'code':
u'nerfeatures', u'features': [{u'code': u'organization', u'name':
u'organization', u'value': 0.0, u'id': 133}, {u'code': u'person', u'name':
u'person', u'value': 0.07142857142857142, u'id': 134}, {u'code': u'gpe',
u'name': u'gpe', u'value': 0.07142857142857142, u'id': 135}, {u'code':
u'location', u'name': u'location', u'value': 0.0, u'id': 136}, {u'code':
u'facility', u'name': u'facility', u'value': 0.0, u'id': 137}], u'id': 7,
u'name': u'NERFeatures'}, {u'code': u'extrafeatures', u'features': [{u'code':
u'dollarsign', u'name': u'dollarsign', u'value': 0.0, u'id': 138}, {u'code':
u'eurosign', u'name': u'eurosign', u'value': 0.0, u'id': 139}, {u'code':
u'poundsign', u'name': u'poundsign', u'value': 0.0, u'id': 140}, {u'code':
u'numbers', u'name': u'numbers', u'value': 0.0, u'id': 141}, {u'code': u'years',
u'name': u'years', u'value': 0.0, u'id': 142}, {u'code': u'englishwords',
u'name': u'englishwords', u'value': 0.9285714285714286, u'id': 143}, {u'code':
u'nonenglishwords', u'name': u'nonenglishwords', u'value': 0.07142857142857142,
u'id': 144}], u'id': 8, u'name': u'ExtraFeatures'},
 {u'code': u'lcm', u'features': [{u'code': u'DAV', u'name': u'DAV', u'value': 0.0, u'id': 145}, 
                                 {u'code': u'IAV', u'name': u'IAV', u'value': 0.0, u'id': 146}, 
                                 {u'code': u'SAV', u'name': u'SAV', u'value': 0.0, u'id': 147}, 
                                 {u'code': u'SV', u'name': u'SV', u'value': 0.0, u'id': 148}, 
                                 {u'code': u'ADJ', u'name': u'ADJ', u'value': 0.0625, u'id': 149}, 
    {u'code': u'abstractness', u'name': u'abstractness', u'value': 4.0, u'id': 150}]
                                 , u'id': 9, u'name': u'LCM'}]}


EXPECTED_RESULT_1 = """ID;theme_cos;theme_count;rain_cos;rain_count;lake_cos;lake_count
1;0.6768353267422083;0.1;0.6573609966232461;0.0666666666667;0.261844912347304;0.0333333333333
"""

EXPECTED_RESULT_2 = """ID;theme_cos;theme_count;symbol_cos;symbol_count;statistic_cos;statistic_count
1;0.2101521053539872;0.015414701484;0.20720323159726212;0.0133440699413;0.023508172957410374;0.00207063154262
"""

EXPECTED_RESULT_3 = """ID;theme_cos;theme_count;north_america_cos;north_america_count;united_state_cos;united_state_count;american_cos;american_count
1;0.45177947603323926;0.181818181818;-0.0016793997815111698;0.0;0.6720305876905618;0.181818181818;0.03378195730198494;0.0
2;0.526462468798382;0.111111111111;0.07199326031561634;0.0;0.044235819871643364;0.0;0.9602491335993789;0.111111111111
3;0.0216011664706142;0.0;0.009831604983560706;0.0;0.022559675793480227;0.0;0.003584210151296649;0.0
4;0.5662809319673751;1.0;1.0;1.0;0.006609358339641354;0.0;0.06213979886523063;0.0
5;0.6759105459609853;1.0;0.006609358339641354;0.0;0.9999999999999998;1.0;0.04776499681412798;0.0
"""


EXPECTED_RESULT_4 = """word;chisq;pval;rate1;rate2;topgroup
fun;13.235294117647058;0.0002747267638079425;0.06841817186644773;1.094690749863164;2
s;10.975609756097562;0.0009231887438696761;1.778872468527641;3.8314176245210723;2
someone;8.333333333333334;0.003892417122778637;0.752599890530925;0.06841817186644773;1
out;7.680851063829787;0.005580984136839307;0.9578544061302683;2.257799671592775;2
cool;7.3478260869565215;0.006714389785549478;1.2315270935960594;0.34209085933223865;1
nt;7.3130434782608695;0.006845588909030592;2.9419813902572525;4.926108374384237;2
and;7.055749128919861;0.007901126114715697;8.278598795840175;11.357416529830322;2
back;6.76;0.009322376047437499;0.4105090311986863;1.299945265462507;2
0;6.428571428571429;0.011229886652916672;1.7104542966611935;0.6841817186644773;1
his;6.2592592592592595;0.01235458501787193;1.3683634373289546;0.47892720306513403;1
that;5.944954128440367;0.014759475366397025;6.226053639846743;8.68910782703886;2
with;5.487804878048781;0.019149571500766092;0.8894362342638205;1.9157088122605361;2
school;5.444444444444445;0.019630657257290702;0.06841817186644773;0.5473453749315819;2
start;5.333333333333333;0.020921335337794035;0.13683634373289547;0.6841817186644773;2
all;5.157894736842105;0.023140931308743732;0.8210180623973727;1.7788724685276414;2
come;4.764705882352941;0.029049022161940597;0.27367268746579093;0.8894362342638202;2
really;4.7407407407407405;0.029456385632571372;2.3946360153256707;1.2999452654625072;1
they;4.7407407407407405;0.029456385632571372;1.299945265462507;2.3946360153256707;2
can;4.666666666666667;0.03075356125927451;1.9157088122605366;0.9578544061302683;1
read;4.545454545454546;0.0330062576612325;0.4105090311986863;1.0946907498631637;2
sit;4.5;0.033894853524689295;0.47892720306513403;0.06841817186644773;1
nothing;4.5;0.033894853524689295;0.06841817186644773;0.47892720306513414;2
most;4.5;0.033894853524689295;0.47892720306513414;0.06841817186644773;1
before;4.5;0.033894853524689295;0.47892720306513403;0.06841817186644773;1
those;4.5;0.033894853524689295;0.06841817186644773;0.47892720306513403;2
00;4.481481481481482;0.034264007734869095;0.5473453749315819;1.299945265462507;2
our;4.454545454545454;0.03480847881186725;0.6157635467980296;0.13683634373289547;1
i;4.432469304229195;0.035261458386858256;23.125342090859334;27.025177887246855;2
to;4.272189349112426;0.03874098467161689;10.262725779967157;12.862616310892172;2
m;4.266666666666667;0.03886710381241731;1.50519978106185;2.5998905309250135;2
what;4.070422535211268;0.04364035668584741;1.847290640394089;3.0103995621237;2
which;4.0;0.04550026389635857;0.8210180623973727;0.27367268746579093;1
than;4.0;0.04550026389635857;0.8210180623973726;0.27367268746579093;1
"""

COSDIST_EXPECTED_RESULT = """;cat;dog;mailman;pizza;sidewalk
cat;1.0;0.18888616673029515;0.20569530512146417;0.04554232231605575;0.10103848254372927
dog;0.18888616673029515;1.0000000000000004;0.12317255882260725;-0.022929349322655924;0.029461587229547823
mailman;0.20569530512146417;0.12317255882260725;0.9999999999999999;-0.04111426242974239;0.17565041830599196
pizza;0.04554232231605575;-0.022929349322655924;-0.04111426242974239;0.9999999999999997;-0.06504301944690698
sidewalk;0.10103848254372927;0.029461587229547823;0.17565041830599196;-0.06504301944690698;1.0000000000000004
"""

NEAREST_NEIGHBORS_EXPECTED_RESULT = [(u'dog', 1.0), (u'dogs', 0.9260612226087319), 
                                     (u'moops', 0.8801252351760882), (u'moop', 0.8801252351760882), 
                                     (u'cur', 0.8165049132149812), (u'manilak', 0.7889894506008646), 
                                     (u'unshopped', 0.788523226869559), (u'puddling', 0.7830698426610627), 
                                     (u'wag', 0.7741565776784584), (u'fencerows', 0.771570759659961)]

BASEDIR = os.path.abspath(os.path.dirname(__file__))

RESULTS_FOLDER = os.path.join(BASEDIR, '..','lingtools_app', 'results')


##################################################
# Tests
##################################################



# def test_single_doc():
#     input_text = "Hello world! My name is Maira and I'm very happy to be here."
#     result = controller.process_text(input_text)
#     #LOG.info(result)
#     
#     assert result == EXPECTED_RESULT
    
def test_problematic_file():
    input_filename = 'MBTI_E_300.txt'
    filetype = 2
    encoding = 'utf-8'
    result_filename = controller.process_file(input_filename, 
                                              filetype=filetype, 
                                              encoding=encoding)
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
      
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
        
    LOG.info(result)
    
    assert result == EXPECTED_RESULT


def test_dynamic_theme():
    theme_name = "Water"
    theme_words = ["rain", "lake", "randomtestword"]
    input_filename = 'test.txt'
    filetype = 3 # file is single document, with empty lines marking new paragraphs
    result_filename = controller.process_dynamic_themes(theme_name, theme_words, input_filename, filetype)    
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
      
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
    #LOG.warning(result)
    assert result == EXPECTED_RESULT_1
      
      
      
  
def test_dynamic_theme_brokenchars():
    theme_name = "Theme"
    theme_words = ["lsa", "symbol", "statistic", "louwerse"]
    input_filename = 'brokenchars.txt'
    encoding = 'auto'
    filetype = 3 # file is single document, with empty lines marking new paragraphs
    result_filename = controller.process_dynamic_themes(theme_name, theme_words, input_filename, filetype, encoding=encoding)
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
       
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
    #LOG.warning(result)
    assert result == EXPECTED_RESULT_2
   
   
   
   
def test_dynamic_theme_ngram():
    theme_name = "USA"
    theme_words = ["north america", "united state", "american"]
    input_filename = 'test_ngram.txt'
    filetype = 1 # 1 document per line
    result_filename = controller.process_dynamic_themes(theme_name, theme_words, input_filename, filetype)    
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
        
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
        
    #LOG.warning(result)
    assert result == EXPECTED_RESULT_3
 
     
     
def test_get_distinctive_words():
    input_filename_1 = 'group1.txt'
    input_filename_2 = 'group2.txt'
    file1_encoding = 'auto'
    file2_encoding = 'auto'
    file1_type = 1 #'txt_multiple' # File is 1 document per line
    file2_type = 1 #'txt_multiple' # File is 1 document per line
    result_values, result_filename = controller.get_distinctive_words(input_filename_1, input_filename_2, 
                                              file1_encoding=file1_encoding, file2_encoding=file2_encoding,
                                              file1_type = file1_type, file2_type = file2_type)
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
    
    # Read file content
    with io.open(result_path, mode="r") as f:
         result = f.read() 
         
    #LOG.warning(result)

    assert result == EXPECTED_RESULT_4
    
    
def test_cosine_dist():
    input_filename = "cosdist_test.txt"
    result_filename = controller.cosine_dists(input_filename, filetype=1, encoding="utf-8")
    
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
    
    # Read file content
    with io.open(result_path, mode="r") as f:
         result = f.read() 
         
    #print(result)
    
    assert result == COSDIST_EXPECTED_RESULT
    
    
def test_nearest_neighbors():
    input = "dog"
    n = 10
    result =  controller.get_nearest_neighbors(input, n)
    
    #print(result)
    
    assert result == NEAREST_NEIGHBORS_EXPECTED_RESULT
    
    
    
def test_nearest_neighbors_wrong():
    input = ["dog"]
    n = 10
    
    with pytest.raises(ValueError):
        result =  controller.get_nearest_neighbors(input, n)
    


