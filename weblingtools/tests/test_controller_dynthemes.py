# -*- coding: utf-8 -*-
'''
Created on 18 apr. 2017

@author: brandaoc
'''
from __future__ import unicode_literals
from __future__ import absolute_import

import pytest
import io
import os

from weblingtools_app import controller


import logging
LOG = logging.getLogger(__name__)

##################################################
# Expected result
##################################################


EXPECTED_RESULT_1 = """ID;theme_cos;theme_count;rain_cos;rain_count;lake_cos;lake_count
1;0.6768353267422083;0.1;0.6573609966232461;0.0666666666667;0.261844912347304;0.0333333333333
"""

EXPECTED_RESULT_2 = """ID;theme_cos;theme_count;symbol_cos;symbol_count;statistic_cos;statistic_count
1;0.2101521053539872;0.015414701484;0.20720323159726212;0.0133440699413;0.023508172957410374;0.00207063154262
"""

EXPECTED_RESULT_3 = """ID;theme_cos;theme_count;north_america_cos;north_america_count;united_state_cos;united_state_count;american_cos;american_count
1;0.45177947603323926;0.181818181818;-0.0016793997815111698;0.0;0.6720305876905618;0.181818181818;0.03378195730198494;0.0
2;0.526462468798382;0.111111111111;0.07199326031561634;0.0;0.044235819871643364;0.0;0.9602491335993789;0.111111111111
3;0.0216011664706142;0.0;0.009831604983560706;0.0;0.022559675793480227;0.0;0.003584210151296649;0.0
4;0.5662809319673751;1.0;1.0;1.0;0.006609358339641354;0.0;0.06213979886523063;0.0
5;0.6759105459609853;1.0;0.006609358339641354;0.0;0.9999999999999998;1.0;0.04776499681412798;0.0
"""


EXPECTED_RESULT_4 = """word;chisq;pval;rate1;rate2;topgroup
fun;13.235294117647058;0.0002747267638079425;0.06841817186644773;1.094690749863164;2
s;10.975609756097562;0.0009231887438696761;1.778872468527641;3.8314176245210723;2
someone;8.333333333333334;0.003892417122778637;0.752599890530925;0.06841817186644773;1
out;7.680851063829787;0.005580984136839307;0.9578544061302683;2.257799671592775;2
cool;7.3478260869565215;0.006714389785549478;1.2315270935960594;0.34209085933223865;1
nt;7.3130434782608695;0.006845588909030592;2.9419813902572525;4.926108374384237;2
and;7.055749128919861;0.007901126114715697;8.278598795840175;11.357416529830322;2
back;6.76;0.009322376047437499;0.4105090311986863;1.299945265462507;2
0;6.428571428571429;0.011229886652916672;1.7104542966611935;0.6841817186644773;1
his;6.2592592592592595;0.01235458501787193;1.3683634373289546;0.47892720306513403;1
that;5.944954128440367;0.014759475366397025;6.226053639846743;8.68910782703886;2
with;5.487804878048781;0.019149571500766092;0.8894362342638205;1.9157088122605361;2
school;5.444444444444445;0.019630657257290702;0.06841817186644773;0.5473453749315819;2
start;5.333333333333333;0.020921335337794035;0.13683634373289547;0.6841817186644773;2
all;5.157894736842105;0.023140931308743732;0.8210180623973727;1.7788724685276414;2
come;4.764705882352941;0.029049022161940597;0.27367268746579093;0.8894362342638202;2
really;4.7407407407407405;0.029456385632571372;2.3946360153256707;1.2999452654625072;1
they;4.7407407407407405;0.029456385632571372;1.299945265462507;2.3946360153256707;2
can;4.666666666666667;0.03075356125927451;1.9157088122605366;0.9578544061302683;1
read;4.545454545454546;0.0330062576612325;0.4105090311986863;1.0946907498631637;2
sit;4.5;0.033894853524689295;0.47892720306513403;0.06841817186644773;1
nothing;4.5;0.033894853524689295;0.06841817186644773;0.47892720306513414;2
most;4.5;0.033894853524689295;0.47892720306513414;0.06841817186644773;1
before;4.5;0.033894853524689295;0.47892720306513403;0.06841817186644773;1
those;4.5;0.033894853524689295;0.06841817186644773;0.47892720306513403;2
00;4.481481481481482;0.034264007734869095;0.5473453749315819;1.299945265462507;2
our;4.454545454545454;0.03480847881186725;0.6157635467980296;0.13683634373289547;1
i;4.432469304229195;0.035261458386858256;23.125342090859334;27.025177887246855;2
to;4.272189349112426;0.03874098467161689;10.262725779967157;12.862616310892172;2
m;4.266666666666667;0.03886710381241731;1.50519978106185;2.5998905309250135;2
what;4.070422535211268;0.04364035668584741;1.847290640394089;3.0103995621237;2
which;4.0;0.04550026389635857;0.8210180623973727;0.27367268746579093;1
than;4.0;0.04550026389635857;0.8210180623973726;0.27367268746579093;1
"""



BASEDIR = os.path.abspath(os.path.dirname(__file__))

RESULTS_FOLDER = os.path.join(BASEDIR, '..','lingtools_app', 'results')


##################################################
# Tests
##################################################



def test_dynamic_theme():
    theme_name = "Water"
    theme_words = ["rain", "lake", "randomtestword"]
    input_filename = 'test.txt'
    filetype = 3 # file is single document, with empty lines marking new paragraphs
    result_filename = controller.process_dynamic_themes(theme_name, theme_words, input_filename, filetype)    
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
      
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
    #LOG.warning(result)
    assert result == EXPECTED_RESULT_1
      
      
      
  
def test_dynamic_theme_brokenchars():
    theme_name = "Theme"
    theme_words = ["lsa", "symbol", "statistic", "louwerse"]
    input_filename = 'brokenchars.txt'
    encoding = 'auto'
    filetype = 3 # file is single document, with empty lines marking new paragraphs
    result_filename = controller.process_dynamic_themes(theme_name, theme_words, input_filename, filetype, encoding=encoding)
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
       
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
    #LOG.warning(result)
    assert result == EXPECTED_RESULT_2
   
   
   
   
def test_dynamic_theme_ngram():
    theme_name = "USA"
    theme_words = ["north america", "united state", "american"]
    input_filename = 'test_ngram.txt'
    filetype = 1 # 1 document per line
    result_filename = controller.process_dynamic_themes(theme_name, theme_words, input_filename, filetype)    
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
        
    # Read file content
    with io.open(result_path, mode="r") as f:
        result = f.read() 
        
    #LOG.warning(result)
    assert result == EXPECTED_RESULT_3
 
     
     
def test_get_distinctive_words():
    input_filename_1 = 'group1.txt'
    input_filename_2 = 'group2.txt'
    file1_encoding = 'auto'
    file2_encoding = 'auto'
    file1_type = 1 #'txt_multiple' # File is 1 document per line
    file2_type = 1 #'txt_multiple' # File is 1 document per line
    result_values, result_filename = controller.get_distinctive_words(input_filename_1, input_filename_2, 
                                              file1_encoding=file1_encoding, file2_encoding=file2_encoding,
                                              file1_type = file1_type, file2_type = file2_type)
    result_path = os.path.join(RESULTS_FOLDER, result_filename)
    
    # Read file content
    with io.open(result_path, mode="r") as f:
         result = f.read() 
         
    LOG.warning(result)

    assert result == EXPECTED_RESULT_4
    
def test_cosine_dist():
    pass
    
    