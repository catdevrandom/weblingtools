'''
Created on 18 apr. 2017

@author: brandaoc
'''

from __future__ import unicode_literals
from __future__ import absolute_import

import sys
import os

import pytest

#import logging
#logging.basicConfig(filename='testrunner.log',level=logging.INFO)


import logging
LOG_FILE = 'weblingtools_testrunner.log'
LOG_LEVEL = logging.DEBUG
formatter = logging.Formatter('%(module)s: %(levelname)s: [%(filename)s:%(lineno)d] %(message).1000s')

#logging.basicConfig(level=LOG_LEVEL, encoding = "utf-8", filename=LOG_FILE)
LOG = logging.getLogger()
LOG.setLevel(LOG_LEVEL)

fh = logging.FileHandler(LOG_FILE, encoding = "utf-8")
fh.setFormatter(formatter)
fh.setLevel(LOG_LEVEL)

LOG.addHandler(fh)

# important locations to add to path
BASEDIR = os.path.abspath(os.path.join('..', os.path.dirname(__name__)))
#LINGTOOLS_LIB = os.path.join(BASEDIR, 'lib', 'lingtools')

sys.path.append(BASEDIR)
#sys.path.append(LINGTOOLS_LIB)


logging.info('Starting the tests...')
#pytest.main(args=['-s', '-vv'])
#pytest.main(args=['test_controller_dynthemes.py', '--capture=sys', '--tb=short', "-vv"])
pytest.main(args=['test_controller.py', '--capture=sys', '--tb=short', '--durations=0', "-vv"])
logging.info('Done with all tests.')