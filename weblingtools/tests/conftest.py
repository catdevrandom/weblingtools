'''
Created on 12 apr. 2017

@author: brandaoc
'''
import pytest
import lingtools_app

'''
@pytest.fixture
def app():
    app = lingtools_app.create_app()
    return app
'''

@pytest.fixture#(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    #settings_override = {
    #    'TESTING': True,
    #    'SQLALCHEMY_DATABASE_URI': TEST_DATABASE_URI
    #}
    #app = lingtools_app.create_app(__name__, settings_override)
    app = lingtools_app.create_app(__name__)

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app

@pytest.fixture#(scope='function')
def client(app):
    return app.test_client()