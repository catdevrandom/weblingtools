'''
Created on 12 apr. 2017

@author: brandaoc
'''
'''
Run the application
'''

import os
import sys


# important locations to add to path
BASEDIR = os.path.abspath(os.path.dirname(__name__))
#LINGTOOLS_LIB = os.path.join(BASEDIR, 'lib', 'lingtools')


sys.path.append(BASEDIR)
#sys.path.append(LINGTOOLS_LIB)

#from sample_application import create_app
from weblingtools_app import create_app

# create an app instance
app = create_app()

app.run(debug=True, host='0.0.0.0', port=5000, use_reloader=False)
app.logger.info("Running from run.py script...")